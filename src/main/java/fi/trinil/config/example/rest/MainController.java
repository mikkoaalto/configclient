package fi.trinil.config.example.rest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

	@Value("${fi.trinil.client.name:default}")
	String value;
	
	@Value("${fi.trinil.client.value:0}")
	Integer intVal;
	
	@RequestMapping("/")
	public String getAutowiredValue() {
		System.out.println("fi.trinil.client.name " + value);
		System.out.println("fi.trinil.client.value " + intVal);
		
		return value;
	}
	
}
