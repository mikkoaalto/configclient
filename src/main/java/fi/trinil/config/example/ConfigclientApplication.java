package fi.trinil.config.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import fi.trinil.config.example.rest.MainController;

@SpringBootApplication
@EnableAutoConfiguration
public class ConfigclientApplication {

	@Autowired
	MainController controller;
	
	public static void main(String[] args) {
		SpringApplication.run(ConfigclientApplication.class, args);
	}
}
